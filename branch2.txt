change branch branch2 => last branch 
$ git status
On branch lastBranch
Your branch is up to date with 'origin/lastBranch'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   branch2.txt

no changes added to commit (use "git add" and/or "git commit -a")

git add branch2.txt 

$ git commit -m "se realizaron los siguintes cambios 
> edit branch2.txt "
[lastBranch 8a133c5] se realizaron los siguintes cambios edit branch2.txt
 1 file changed, 10 insertions(+), 1 deletion(-)


 $ git reset --soft 
 undo last commit 

(branch3)
$ git merge branch2
Updating 47a2b91..eef5b8f
Fast-forward
 branch.txt      | 17 ++++++++++++++++-
 branch2.txt     | 25 ++++++++++++++++++++++++-
 fix.txt         |  1 +
 otherCommit.txt |  1 +
 4 files changed, 42 insertions(+), 2 deletions(-)
 create mode 100644 fix.txt
 create mode 100644 otherCommit.txt
